//
//  ViewController.swift
//  Collector
//
//  Created by Pardip Bhatti on 09/04/17.
//  Copyright © 2017 gpCoders. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    
    @IBOutlet weak var tableViews: UITableView!

    var games: [Game] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableViews.delegate = self
        tableViews.dataSource = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        let context = (UIApplication.shared.delegate as! AppDelegate)
        
        do {
            games = try context.persistentContainer.viewContext.fetch(Game.fetchRequest())
            tableViews.reloadData()
        } catch {
            print("We have errors here");
        }
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return games.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell()
        let game = games[indexPath.row]
        cell.textLabel?.text = game.title
        cell.imageView?.image = UIImage(data: game.image! as Data)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let game = games[indexPath.row]
        performSegue(withIdentifier: "gameSegue", sender: game)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let nextVC = segue.destination as! GameViewController
        nextVC.game = sender as? Game
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Deleting from core data
            let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
            context.delete(games[indexPath.row])
            (UIApplication.shared.delegate as! AppDelegate).saveContext()
            
            // Deleting row from table view
            games.remove(at: indexPath.row)
            tableViews.deleteRows(at: [indexPath], with: .bottom)
            
        }
    }
    
}

